var drawChessBoard = function (sizeBoard) {
    var black = '██  ';
    var white = '  ██';
    var firstItem = '';
    var nextItem = '';
    var flag = false;

    for (var i = 0; i < sizeBoard / 2; i++) {
        firstItem += black;
        nextItem += white;
    }

    for (var j = 0; j < sizeBoard; j++) {
        if (j === 0) {
            flag = !flag;
        }
        if (flag === true) {
            console.log(nextItem);
            flag = false;
        } else if (flag === false) {
            console.log(firstItem);
            flag = true;
        }
    }
};
console.log(drawChessBoard(8));